<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/key', function() {
    return \Illuminate\Support\Str::random(32);
});

// $router->post('auth/register','UserController@register');

$router->group(['prefix' => 'user', 'namespace' => 'Master'], function () use ($router) {
    $router->get('show_all', 'UserController@show_all');
    $router->get('show/{id}', 'UserController@show');
    $router->post('store', 'UserController@store');
    $router->delete('destroy/{id}', 'UserController@destroy');
    $router->put('update/{id}', 'UserController@update');
});
