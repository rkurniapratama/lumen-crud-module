<?php

namespace App\Helpers;

use DB;

class BaseEloquent
{
    public static function showAll($model, $select = null)
    {
        $result = new \stdClass;
        $query = array();
        try 
        {
            if($select == null)
            {
                $query = $model::all();
            }
            else 
            {
                $query = $model::select($select)->get();
            }

            $result->status = 'S';
            $result->message = 'Successfully';
            $result->data = $query;
        }
        catch (\Exception $ex)
        {
            $result->status = 'E';
            $result->message = $ex->getMessage();
            $result->data = $query;
        }
        return $result;
    }

    public static function show($model, $where, $select = null)
    {
        $result = new \stdClass;
        $query = array();
        try 
        {
            if($select == null)
            {
                $query = $model::where($where)->first();
            }
            else 
            {
                $query = $model::where($where)->select($select)->first();
            }

            $result->status = 'S';
            $result->message = 'Successfully';
            $result->data = $query;
        }
        catch (\Exception $ex)
        {
            $result->status = 'E';
            $result->message = $ex->getMessage();
            $result->data = $query;
        }
        return $result;
    }

    public static function store($model, $data)
    {
        DB::beginTransaction();
        $result = new \stdClass;
        try 
        {
            $query = $model::create($data);

            DB::commit();

            $result->status = 'S';
            $result->message = 'Successfully';
        }
        catch (\Exception $ex)
        {
            DB::rollback();
            $result->status = 'E';
            $result->message = $ex->getMessage();
        }
        return $result;
    }

    public static function update($model, $where, $data)
    {
        DB::beginTransaction();
        $result = new \stdClass;
        try 
        {
            $query = $model::where($where)->update($data);

            DB::commit();

            $result->status = 'S';
            $result->message = 'Successfully';
        }
        catch (\Exception $ex)
        {
            DB::rollback();
            $result->status = 'E';
            $result->message = $ex->getMessage();
        }
        return $result;
    }

    public static function destroy($model, $where)
    {
        DB::beginTransaction();
        $result = new \stdClass;
        try 
        {
            $query = $model::where($where)->delete();

            DB::commit();

            $result->status = 'S';
            $result->message = 'Successfully';
        }
        catch (\Exception $ex)
        {
            DB::rollback();
            $result->status = 'E';
            $result->message = $ex->getMessage();
        }
        return $result;
    }
}