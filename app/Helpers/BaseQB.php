<?php

namespace App\Helpers;

use DB;

class BaseQB
{
    public static function showAll($tableName, $select = null)
    {
        $result = new \stdClass;
        $query = array();
        try 
        {
            if($select == null)
            {
                $query = DB::table($tableName)->get();
            }
            else 
            {
                $query = DB::table($tableName)->select($select)->get();
            }

            $result->status = 'S';
            $result->message = 'Successfully';
            $result->data = $query;
        }
        catch (\Exception $ex)
        {
            $result->status = 'E';
            $result->message = $ex->getMessage();
            $result->data = $query;
        }
        return $result;
    }

    public static function show($tableName, $where, $select = null)
    {
        $result = new \stdClass;
        $query = array();
        try 
        {
            if($select == null)
            {
                $query = DB::table($tableName)->where($where)->first();
            }
            else 
            {
                $query = DB::table($tableName)->where($where)->select($select)->first();
            }

            $result->status = 'S';
            $result->message = 'Successfully';
            $result->data = $query;
        }
        catch (\Exception $ex)
        {
            $result->status = 'E';
            $result->message = $ex->getMessage();
            $result->data = $query;
        }
        return $result;
    }

    public static function store($tableName, $data)
    {
        DB::beginTransaction();
        $result = new \stdClass;
        try 
        {
            $query = DB::table($tableName)->insert($data);

            DB::commit();

            $result->status = 'S';
            $result->message = 'Successfully';
        }
        catch (\Exception $ex)
        {
            DB::rollback();
            $result->status = 'E';
            $result->message = $ex->getMessage();
        }
        return $result;
    }

    public static function update($tableName, $where, $data)
    {
        DB::beginTransaction();
        $result = new \stdClass;
        try 
        {
            $query = DB::table($tableName)->where($where)->update($data);

            DB::commit();

            $result->status = 'S';
            $result->message = 'Successfully';
        }
        catch (\Exception $ex)
        {
            DB::rollback();
            $result->status = 'E';
            $result->message = $ex->getMessage();
        }
        return $result;
    }

    public static function destroy($tableName, $where)
    {
        DB::beginTransaction();
        $result = new \stdClass;
        try 
        {
            $query = DB::table($tableName)->where($where)->delete();

            DB::commit();

            $result->status = 'S';
            $result->message = 'Successfully';
        }
        catch (\Exception $ex)
        {
            DB::rollback();
            $result->status = 'E';
            $result->message = $ex->getMessage();
        }
        return $result;
    }
}