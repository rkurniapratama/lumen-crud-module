<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Helpers\BaseQB;
use App\Models\User;
use DB;

class UserController extends Controller
{
    // public function register(Request $request)
    // {
    //     $this->validate($request, [
    //         'name' => 'required|string|max:255',
    //         'email' => 'required|string|email|max:255|unique:users',
    //         'password' => 'required|string|min:8|confirmed',
    //     ]);

    //     $user = new User();
    //     $user->name = $request->name;
    //     $user->email = $request->email;
    //     $user->password = Hash::make($request->password);
    //     $user->save();
      
    //     /**Take note of this: Your user authentication access token is generated here **/
    //     $data['token'] =  $user->createToken('MyApp')->accessToken;
    //     $data['user'] =  $user;

    //     return response(['data' => $data, 'message' => 'Account created successfully!', 'status' => true]);
    // }

    public function show_all() 
    {
        try
        {
            $baseqb = BaseQB::showAll('users', ['id', 'name', 'email', 'last_logged_in']);
            if($baseqb->status != 'S') 
            {
                throw new \Exception($baseqb->message);
            }
            return response()->json($baseqb);
        }
        catch (\Exception $ex)
        {
            return response()->json(['status' => 'E', 'message' => $ex->getMessage()], 500);
        }
    }

    public function show($id)
    {
        try
        {
            $baseqb = BaseQB::show('users', ['id' => $id], ['id', 'name', 'email', 'last_logged_in']);
            if($baseqb->status != 'S') 
            {
                throw new \Exception($baseqb->message);
            }
            return response()->json($baseqb);
        }
        catch (\Exception $ex)
        {
            return response()->json(['status' => 'E', 'message' => $ex->getMessage()], 500);
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ]);
        
        try
        {
            $data['name'] = $request->name;
            $data['email'] = $request->email;
            $data['password'] = Hash::make($request->password);
            $data['created_at'] = \Carbon\Carbon::now();
            $data['updated_at'] = \Carbon\Carbon::now();
            $baseqb = BaseQB::store('users', $data);
            if($baseqb->status != 'S') 
            {
                throw new \Exception($baseqb->message);
            }
            return response()->json($baseqb);
        }
        catch (\Exception $ex)
        {
            return response()->json(['status' => 'E', 'message' => $ex->getMessage()], 500);
        }
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ]);

        try
        {
            $data['name'] = $request->name;
            $data['email'] = $request->email;
            $data['password'] = Hash::make($request->password);
            $data['updated_at'] = \Carbon\Carbon::now();
            $baseqb = BaseQB::update('users', ['id' => $id], $data);
            if($baseqb->status != 'S') 
            {
                throw new \Exception($baseqb->message);
            }
            return response()->json($baseqb);
        }
        catch (\Exception $ex)
        {
            return response()->json(['status' => 'E', 'message' => $ex->getMessage()], 500);
        }
    }
    
    public function destroy($id)
    {
        try
        {
            $baseqb = BaseQB::destroy('users', ['id' => $id]);
            if($baseqb->status != 'S') 
            {
                throw new \Exception($baseqb->message);
            }
            return response()->json($baseqb);
        }
        catch (\Exception $ex)
        {
            return response()->json(['status' => 'E', 'message' => $ex->getMessage()], 500);
        }
    }
}