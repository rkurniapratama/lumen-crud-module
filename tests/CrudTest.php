<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class CrudTest extends TestCase
{
    public function testShowAllUser()
    {
        $this->get('user/show_all')
        ->seeStatusCode(200)
        ->seeJsonStructure([
            'status',
            'message',
            'data' => ['*' =>
                [
                    'id',
                    'name',
                    'email',
                    'last_logged_in'
                ]
            ]
        ]);
    }

    public function testShowUser()
    {
        $this->get('user/show/5')
        ->seeStatusCode(200)
        ->seeJsonStructure([
            'status',
            'message',
            'data' => [
                'id',
                'name',
                'email',
                'last_logged_in'
            ]
        ]);
    }

    public function testStoreUser()
    {
        $param = [
            'name' => 'Tukiem',
            'email' => 'tukiem@gmail.com',
            'password' => 'tukiem123',
            'password_confirmation' => 'tukiem123'
        ];
        $this->post('user/store', $param)
        ->seeStatusCode(200)
        ->seeJsonStructure([
            'status',
            'message'
        ]);
    }

    public function testUpdateUser()
    {
        $param = [
            'name' => 'Sri',
            'email' => 'sri@gmail.com',
            'password' => 'sri12345'
        ];
        $this->put('user/update/7', $param)
        ->seeStatusCode(200)
        ->seeJsonStructure([
            'status',
            'message'
        ]);
    }

    public function testDestroyUser()
    {
        $this->delete('user/destroy/7')
        ->seeStatusCode(200)
        ->seeJsonStructure([
            'status',
            'message'
        ]);
    }
}
